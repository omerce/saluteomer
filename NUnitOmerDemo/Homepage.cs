using NHibernate.Mapping;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
namespace NUnitOmerDemo
{
    public class Homepage
    {
        [SetUp]
        public void Setup()
        {

        }
        [Test]
        [TestCase("kopruluomerfaruk@gmail.com", "valori", true)] //Testcase 
        public void logInTest(string email, string password, bool isAccepted)
        {
                IWebDriver driver = new ChromeDriver();
                driver.Navigate().GoToUrl("https://webshop.mobiletestautomation.nl/");
                
            var subscribeNews = driver.FindElement(By.Name("email"));
            var emailSubmit = driver.FindElement(By.XPath(".//*[@value='Subscribe' or @value='OK']"));
            subscribeNews.SendKeys("kopruluomerfaruk@gmail.com");
            //emailSubmit.Click();

            
            //Login
            IWebElement signIn = driver.FindElement(By.XPath(".//*[@id='_desktop_user_info']"));
            signIn.Click();
            var txtEmail = driver.FindElement(By.Name("email"));
            var txtPassword = driver.FindElement(By.Name("password"));
            var txtSubmit = driver.FindElement(By.XPath(".//*[@id='submit-login']"));

            txtEmail.SendKeys("kopruluomerfaruk@gmail.com");
            txtPassword.SendKeys("valori");
            txtSubmit.Click();
            Console.WriteLine("Succeed Sendkeyss");

            /*
            //Assertion if user can log in
            if (isAccepted)
            {
                Assert.That(driver.Url, Is.EqualTo(driver.Url + "/my-account"));
            }
            else
            {
                Assert.That(driver.FindElement(By.Name("ErrorBox")).Text, Is.EqualTo("Login failed"));
            }
            }
            */



        //Adding clothing to cart 
        IWebElement clothes = driver.FindElement(By.XPath(".//*[@id='category-3']"));
        clothes.Click();

        var cloth1 = driver.FindElement(By.XPath(".//a[@href='https://webshop.mobiletestautomation.nl/men/1-1-hummingbird-printed-t-shirt.html#/1-size-s/8-color-white']"));
        cloth1.Click();

        var submitCart = driver.FindElement(By.XPath(".//*[@class='btn btn-primary add-to-cart']"));
        submitCart.Click();

            /*
            //Deze code heb ik gevonden voor het handlen van pop-up scherm zodat ik hier de Sendkey kan meegeven
            // , maar ik kan het helaas niet gebruiken. 

            Set<String> handles = driver.getWindowHandles();
            Iterator<String> it = handles.iterator();

            String parent = it.next();
            String child = it.next();

            driver.SwitchTo().Window(child);

            //perform actions

            driver.Close(); // only for child


            driver.SwitchTo().Window(parent);

           */

            var proceedToCart = driver.FindElement(By.XPath(".//a[@href='https://webshop.mobiletestautomation.nl/cart?action=show']"));
        proceedToCart.Click();

     
    }
    }
}
